package apiserver

import (
	"github.com/gin-gonic/gin"
	"fmt"
)

func StartServer(ip string, firstString string, secondString string) {
	fmt.Println("Rest API Golang")

	r := gin.Default()
	r.GET("/", StringCont(firstString, secondString))
	r.Run("localhost:5000")
}

func StringCont(c *gin.Context, firstString string, secondString string) {
	c.JSON(200, gin.H{
		"firstString":  firstString,
		"secondString": secondString,
		"result":       firstString + secondString,
	})
}
